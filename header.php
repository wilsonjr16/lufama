<!DOCTYPE html>
<html>
<head>
		<title><?php print $cabecalho_title; ?>Lu'Fama Fashion</title>
		<meta name="viewport" content="width=device-width">
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/mobile.css">
		<link rel="stylesheet" href="css/sobre.css">
		<link rel="stylesheet" href="css/produto.css">
		<link rel="stylesheet" src="http://fonts.googleapis.com/css?family=PT+Sans|Bad+Script">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16" />
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<![endif]-->
</head>
    
<body>
        
<header class="main-header">
	<div class="header-top">
		<div class="grid">
			<nav class="menu-opcoes">
			<ul>
				<li class="option-list"><a href="#">Sua conta</a></li>
				<li class="option-list"><a href="#">Lista de desejos</a></li>
				<li class="option-list"><a href="#">Cartão fidelidade</a></li>
				<li class="option-list"><a href="sobre.php">Sobre</a></li>
				<li class="option-list"><a href="#">Ajuda</a></li>
			</ul>
			</nav>
		</div>
	</div>

	<div class="grid header-midle">		 	
		<a href="index.html"><h1 class="logo"><img src="img/logo-lufama.png" alt="Lufama fashion"></h1></a>

		<form class="search">
			<input type="search" class="search_text">
			<input type="image" src="img/busca.png" class="lupa">    
		</form>

		<p class="sacola">Nenhum item na sacola de compras</p>			
	</div>

	<nav class="nav-dep header-bottom">
		<ul class="grid main-menu">
			<li class="list-item-dep">
				<a href="#">Blusas e Camisas</a>
				<ul class="sub-menu-dep">
						<li class="sub-menu-depitem"><a class="sub-menu-deplink" href="#">Manga curta</a></li>
						<li class="sub-menu-depitem"><a class="sub-menu-deplink" href="#">Manga comprida</a></li>
						<li class="sub-menu-depitem"><a class="sub-menu-deplink" href="#">Camisa social</a></li>
						<li class="sub-menu-depitem"><a class="sub-menu-deplink" href="#">Camisa casual</a></li>
				</ul>
			</li>
			<li class="list-item-dep"><a href="#">Calças</a></li>
			<li class="list-item-dep"><a href="#">Saias</a></li>
			<li class="list-item-dep"><a href="#">Vestidos</a></li>
			<li class="list-item-dep"><a href="#">Sapatos</a></li>
			<li class="list-item-dep"><a href="#">Bolsas e Carteiras</a></li>
			<li class="list-item-dep"><a href="#">Acessórios</a></li>
		</ul>
	</nav>
</header>


<header class="header_mobile">
	<div class="hamburguer">
        <div class="hamburguer_item first"></div>
        <div class="hamburguer_item middle"></div>
        <div class="hamburguer_item last"></div>
    </div>

    <h1 class="logo_mobile">
			<a class="logo_mobile-link" href="index.html">
				<img src="img/logo-lufama.png" alt="imagem da logo" class="img_logo-mobile img-responsive center-block"/>
			</a>
    </h1>
    
    <div class="nav_mobile_wrapper">

      <div class="nav_mobile_col fleft">
				<h1 class="logo_mobile">
					<a class="logo_mobile-link" href="index.html">
						<img src="img/logo-lufama.png" alt="imagem da logo" class="img_logo-mobile img-responsive center-block"/>
					</a>
				</h1>
				
				<div class="menu_close"></div>

        <div class="borda-divisao"></div>

        <nav class="header_mobile-nav">

          <ul class="header_menu">
              <li class="header_menu-item"><a href="index.php" class="header_menu-link">HOME</a></li>

              <li class="header_menu-item"><a href="sobre.php" class="header_menu-link">SOBRE</a></li>

              <li class="header_menu-item"><a href="nossa-historia.php" class="header_menu-link">Calças</a></li>

              <li class="header_menu-item"><a href="consultoria-estrategica.php" class="header_menu-link">Saias</a></li>

              <li class="header_menu-item"><a href="consultoria-financeira.php" class="header_menu-link">Vestidos</a></li>

              <li class="header_menu-item"><a href="metodo-mude.php" class="header_menu-link">Sapatos</a></li>

              <li class="header_menu-item"><a href="equipe.php" class="header_menu-link">Bolsas e Carteiras</a></li>

              <li class="header_menu-item"><a href="clientes.php" class="header_menu-link">Acessórios</a></li>

          </ul>

        </nav>
			</div>
			
	</div>
</header>