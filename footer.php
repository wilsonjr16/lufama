<footer>
        <div class="grid">
            <img src="img/Logo-footer.png" alt="Logo mirror Fashion">
            
            <ul class="social">
                <li><a href="http://facebook.com/mirrorfashion">Facebook</a></li>
                <li><a href="http://twitter.com/mirrorfashion">Twitter</a></li>
                <li><a href="http://plus.google.com/mirrorfashion">Google+</a></li>
            </ul>
        </div>
    </footer>
    <script src="js/jquery.js"></script>
    <script src="js/menu_mob.js"></script>
</html>