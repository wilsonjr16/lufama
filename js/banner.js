var banners = ["img/banner1.jpeg","img/banner2.jpeg","img/banner3.jpeg"];
var bannerAtual =0;

function trocarBanner(){
    bannerAtual = (bannerAtual + 1) % 3;
    document.querySelector('.destaque img').src = banners[bannerAtual];
}

//setInterval(trocarBanner, 4000);

var timer = setInterval(trocarBanner, 4000);
var controle = document.querySelector('.pause');

controle.onclick = function() {
    if (controle.className == 'pause') {
        clearInterval(timer);
        controle.className = 'play';
    } else {
        timer = setInterval(trocarBanner, 4000);
        controle.className = 'pause';
    }
    
    return false;
};